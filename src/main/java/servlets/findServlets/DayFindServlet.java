package servlets.findServlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@WebServlet(name = "DayFindServlet", urlPatterns = "/dayFind")
public class DayFindServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BufferedReader br = new BufferedReader(
                new InputStreamReader(req.getInputStream()));
        String json = "";

        if(br != null){
            json = br.readLine();
        }

        System.out.println(json);


        req.getSession().setAttribute("dayNote", json);
        System.out.println("KERVA");
        System.out.println(req.getSession().getAttribute("dayNote"));
    }
}
