package servlets.findServlets;

import orm.NotesDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "ChangeStatusNoteServlet", urlPatterns = "/changeStatus")
public class ChangeStatusNoteServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String status = req.getSession().getAttribute("noteStatus").toString();

        int idNote = Integer.parseInt(req.getSession().getAttribute("idNote").toString());

        NotesDAO notesDAO = new NotesDAO();
        notesDAO.changeNoteStatus(idNote, status);

        req.getRequestDispatcher("index.html").forward(req, resp);

    }
}
