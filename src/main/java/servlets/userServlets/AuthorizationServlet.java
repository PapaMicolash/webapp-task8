package servlets.userServlets;

import model.User;
import orm.UsersDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;
import java.io.IOException;

@WebServlet(name = "AuthorizationServlet", urlPatterns = "/auth")
public class AuthorizationServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");

        HttpSession session = request.getSession();

        UsersDAO usersDAO = new UsersDAO();

        User authUser = usersDAO.getUserByAuthorization(login, password);

        if (authUser != null) {
            session.setAttribute("login", authUser.getLogin());
            session.setAttribute("password", authUser.getPassword());
            session.setAttribute("id", authUser.getId());
            request.getRequestDispatcher("index.html").forward(request, response);
        } else {
            if (session.getAttribute("login") != null & session.getAttribute("password") != null) {
                request.getRequestDispatcher("index.html").forward(request, response);
            } else {
                request.getRequestDispatcher("authorization.html").forward(request, response);
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
