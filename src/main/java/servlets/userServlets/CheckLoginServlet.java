package servlets.userServlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "CheckLoginServlet", urlPatterns = "/checkLogin")
public class CheckLoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        if (session != null && session.getAttribute("login") != null
                && session.getAttribute("password") != null) {

            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write("1");
        }
        else {

            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write("0");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
