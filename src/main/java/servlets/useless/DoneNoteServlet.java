package servlets.useless;

import orm.NotesDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


@WebServlet(name = "DoneNoteServlet", urlPatterns = "/done")
public class DoneNoteServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String status = "DONE";

        int idNote = Integer.parseInt(req.getSession().getAttribute("idNote").toString());
        System.out.println(idNote);

        NotesDAO notesDAO = new NotesDAO();
        notesDAO.changeNoteStatus(idNote, status);

        req.getRequestDispatcher("index.html").forward(req, resp);
    }
}