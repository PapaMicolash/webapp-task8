package servlets.fileWorkersServlets;


import orm.NotesDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

@WebServlet(name = "FileDownloadServlet", urlPatterns = "/FileDownload")
public class FileDownloadServlet extends HttpServlet {

    private static final String DIRPATH = System.getProperty("user.dir") + File.separator + "output" + File.separator;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        int idNote = Integer.parseInt(req.getSession().getAttribute("idNote").toString());
        System.out.println(idNote);


        NotesDAO notesDAO = new NotesDAO();
        req.getSession().removeAttribute("idNote");

        String fileName = notesDAO.getFileName(idNote);
        FileInputStream fileInputStream = null;
        OutputStream responseOutputStream = null;
        try
        {

            String FILEPATH = DIRPATH + fileName;
            Path path = Paths.get(DIRPATH + fileName);
            System.out.println(FILEPATH);

            if (Files.exists(path)) {
                File file = new File(FILEPATH);

                String mimeType = req.getServletContext().getMimeType(FILEPATH);
                if (mimeType == null) {
                    mimeType = "application/octet-stream";
                }
                resp.setContentType(mimeType);
                resp.addHeader("Content-Disposition", "attachment; filename=" + fileName);
                resp.setContentLength((int) file.length());

                fileInputStream = new FileInputStream(file);
                responseOutputStream = resp.getOutputStream();
                int bytes;
                while ((bytes = fileInputStream.read()) != -1) {

                    responseOutputStream.write(bytes);
                }
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            fileInputStream.close();
            responseOutputStream.close();
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        BufferedReader br = new BufferedReader(
                new InputStreamReader(req.getInputStream()));
        String json = "";

        if(br != null){
            json = br.readLine();
            System.out.println(br.readLine());
            System.out.println(br.toString());
            System.out.println(json);
        }
    }
}
