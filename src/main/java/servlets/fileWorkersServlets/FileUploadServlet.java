package servlets.fileWorkersServlets;

import model.Note;
import orm.NotesDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


@WebServlet(name = "FileUploadServlet", urlPatterns = "/FileUpload")
@MultipartConfig
public class FileUploadServlet extends HttpServlet {
    private static final String FILENAME = System.getProperty("user.dir") + File.separator + "output" + File.separator;
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Hello FileUpload");
        int idNote = Integer.parseInt(req.getParameter("petuh"));
        NotesDAO nd = new NotesDAO();
        System.out.println(nd.checkFile(idNote));

        if (nd.checkFile(idNote)) {
            String fileName = nd.getFileName(idNote);
            File file = new File(FILENAME + fileName);
            file.delete();
            System.out.println("File has been deleted!");
        }

        try {
            Part partAttr = req.getPart("file" + idNote);
            InputStream is = partAttr.getInputStream();

            System.out.println(partAttr.getSubmittedFileName());

            Path getPath = Paths.get(FILENAME);

            if (!Files.exists(getPath)) {
                File output = new File(FILENAME);
                output.mkdir();
            }


            String pathName = FILENAME + partAttr.getSubmittedFileName();
            System.out.println(pathName);

            System.out.println("hello File!!");


            File file = new File(pathName);
            FileOutputStream outputStream = new FileOutputStream(file);
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = is.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
            outputStream.close();
            is.close();


            nd.uploadFile(idNote, file.getName());

        } catch(IOException ex) {
            System.out.println(ex);
        }

        System.out.println(req.getParameter("petuh"));
        req.getRequestDispatcher("index.html").forward(req, resp);

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
