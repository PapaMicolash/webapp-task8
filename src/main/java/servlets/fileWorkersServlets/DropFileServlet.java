package servlets.fileWorkersServlets;


import orm.NotesDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@WebServlet(name = "DropFileServlet", urlPatterns = "/dropFile")
public class DropFileServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("dropFile");
        String status = "DONE";

        int idNote = Integer.parseInt(req.getSession().getAttribute("idNote").toString());

        NotesDAO notesDAO = new NotesDAO();
        notesDAO.dropFileFromNote(idNote);
        req.getRequestDispatcher("index.html").forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
