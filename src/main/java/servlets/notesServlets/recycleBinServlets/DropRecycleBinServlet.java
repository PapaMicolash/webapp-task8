package servlets.notesServlets.recycleBinServlets;

import orm.NotesDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "DropRecycleBinServlet", urlPatterns = "/dropRB")
public class DropRecycleBinServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int idUser = Integer.parseInt(req.getSession().getAttribute("id").toString());

        NotesDAO notesDAO = new NotesDAO();
        notesDAO.dropRecycleBin(idUser);


        req.getRequestDispatcher("index.html").forward(req, resp);
    }
}
