package servlets.notesServlets;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import model.Note;
import model.NoteDeserializer;
import model.User;
import orm.NotesDAO;
import orm.UsersDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

@WebServlet(name = "AddNoteServlet", urlPatterns = "/addNote")
public class AddNoteServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int idUser = Integer.parseInt(request.getSession().getAttribute("id").toString());

        BufferedReader br = new BufferedReader(
                new InputStreamReader(request.getInputStream()));
        String json = "";

        if (br != null) {
            json = br.readLine();
            System.out.println("222");
        }

        System.out.println(request.getSession().getAttribute("login").toString() + " " + request.getSession().getAttribute("password").toString());
        System.out.println(json);
        System.out.println(request.getSession().getAttribute("login").toString() + " " + request.getSession().getAttribute("password").toString());

        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Note.class, new NoteDeserializer());
        Gson gson = builder.create();
        Note note = gson.fromJson(json, Note.class);

        System.out.println("NoteGSON isHERE!!");

        System.out.println(request.getSession().getAttribute("login").toString() + " " + request.getSession().getAttribute("password").toString());

        System.out.println(note.toString());

        if (!note.checkFields()) {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write("1");
        } else {
            note.setIdUser(idUser);
            note.setStatus("ACTIVE");
            NotesDAO notesDAO = new NotesDAO();
            notesDAO.insertNote(note);

            System.out.println("Insert new note!!!");
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write("2");
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
