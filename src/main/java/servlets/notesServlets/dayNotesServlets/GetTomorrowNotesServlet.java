package servlets.notesServlets.dayNotesServlets;

import com.google.gson.Gson;
import model.Note;
import orm.NotesDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "GetTomorrowNotesServlet", urlPatterns = "/tomorrow")
public class GetTomorrowNotesServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int idUser = Integer.parseInt(req.getSession().getAttribute("id").toString());

        NotesDAO notesDAO = new NotesDAO();
        notesDAO.sortNotesOnDate(idUser);

        ArrayList<Note> tomorrowNotes = notesDAO.getTomorrowNotes();

        Gson gson = new Gson();
        String tomorrowNotesJSON = gson.toJson(tomorrowNotes);

        if (tomorrowNotesJSON != null) {
            resp.setContentType("application/json");
            resp.setCharacterEncoding("UTF-8");
            resp.getWriter().write(tomorrowNotesJSON);
        }
    }
}
