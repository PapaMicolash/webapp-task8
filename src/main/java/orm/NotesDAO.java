package orm;

import model.Note;

import javax.persistence.Id;
import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;

public class NotesDAO extends DAO {
    public static final String SELECT_NOTE_FROM_ID =
            "SELECT *  from notes WHERE id = ?";
    public static final String SELECT_NOTES_FROM_ID_USER =
            "SELECT *  from notes WHERE id_user = ? and status = ?";
    public static final String SELECT_NOTES_FOR_TODAY =
            "SELECT * from notes WHERE status = ? and id_user = ? and date <= ?";
    public static final String SELECT_NOTES_FOR_TOMORROW =
            "SELECT * from notes WHERE status = ? and id_user = ? and date = ?";
    public static final String SELECT_NOTES_FOR_SOMEDAY =
            "SELECT * from notes WHERE status = ? and id_user = ? and date > ?";
    public static final String FILENAME = System.getProperty("user.dir") + File.separator + "output" + File.separator;

    Connection connection;
    PreparedStatement preparedStatementInsert;

    ArrayList<Note> tomorrowNotes = new ArrayList<>();
    ArrayList<Note> todayNotes = new ArrayList<>();
    ArrayList<Note> somedayNotes = new ArrayList<>();
    ArrayList<Note> recycleBin = new ArrayList<>();

    public ArrayList<Note> getTomorrowNotes() {
        return tomorrowNotes;
    }

    public ArrayList<Note> getTodayNotes() {
        return todayNotes;
    }

    public ArrayList<Note> getSomedayNotes() {
        return somedayNotes;
    }

    public void setRecycleBin(ArrayList<Note> recycleBin) {
        this.recycleBin = recycleBin;
    }

    public ArrayList<Note> getRecycleBin() {
        return recycleBin;
    }

    public NotesDAO() {
        try{
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Connection successful!");
        }
        catch(Exception ex){
            System.out.println("Connection failed...");
            System.out.println(ex);
        }

        try {
            connection = DriverManager.getConnection(DBURL, USERNAME, PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



    public void sortNotesOnDate(int user) {
        java.util.Date date = new java.util.Date();
        Date currentDate = new Date(date.getTime());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, 1);
        java.util.Date tom = calendar.getTime();
        Date tomorrow = new Date(tom.getTime());
        sortNotesFromSQL(user, SELECT_NOTES_FOR_TODAY, currentDate);
        sortNotesFromSQL(user, SELECT_NOTES_FOR_TOMORROW, tomorrow);
        sortNotesFromSQL(user, SELECT_NOTES_FOR_SOMEDAY, currentDate);
    }


    public void sortNotesFromSQL(int user, String SQL, Date date) {
        int id;
        Note note = null;
        int idUser;
        Date dateS;
        String text;
        String status;
        String fileName;

        PreparedStatement pr = null;
        try {
            pr = connection.prepareStatement(SQL);
            pr.setString(1, "ACTIVE");
            pr.setInt(2, user);
            pr.setDate(3, date);
            ResultSet rs = pr.executeQuery();
            while (rs.next()) {
                id = rs.getInt(1);
                idUser = rs.getInt(2);
                dateS = rs.getDate(3);
                text = rs.getString(4);
                status = rs.getString(5);
                fileName = rs.getString(6);
                note = new Note(id, idUser, dateS, text, status, fileName);

                if (SQL.equals(SELECT_NOTES_FOR_TODAY)) {
                    todayNotes.add(note);
                } else if (SQL.equals(SELECT_NOTES_FOR_TOMORROW)) {
                    tomorrowNotes.add(note);
                } else if (SQL.equals(SELECT_NOTES_FOR_SOMEDAY)) {
                    somedayNotes.add(note);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Note> getNotesByIDUser(int idUser, String noteStatus) {
        ArrayList<Note> userNotes = new ArrayList<>();
        Note note;
        int id;
        Date date;
        String text;
        String status;
        String fileName;

        try {
            PreparedStatement pr = connection.prepareStatement("SELECT *  from notes WHERE id_user = ? and status = ?");
            pr.setInt(1, idUser);
            pr.setString(2,noteStatus);
            ResultSet rs = pr.executeQuery();
            while (rs.next()) {
                id = rs.getInt(1);
                date = rs.getDate(3);
                text = rs.getString(4);
                status = rs.getString(5);
                fileName = rs.getString(6);
                note = new Note(id, idUser, date, text, status, fileName);
                userNotes.add(note);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userNotes;
    }

    public boolean checkFile(int IdNote) {
        String fileName = null;
        fileName = getFileName(IdNote);
        if (fileName != "")  {
            return true;
        }
        return false;
    }

    public String getFileName(int IdNote) {
        String fileName = null;
        try {
            PreparedStatement pr = connection.prepareStatement("SELECT file " +
                    "FROM notes WHERE id = ?");
            pr.setInt(1, IdNote);
            ResultSet rs = pr.executeQuery();
            while(rs.next()) {
                fileName = rs.getString("file");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return fileName;
    }

    public void insertNote(Note note) {
        UsersDAO usersDAO = new UsersDAO();

        if (usersDAO.getUserById(note.getIdUser()) != null) {
            try {
                PreparedStatement pr = connection.prepareStatement(
                        "INSERT INTO notes(id_user, date, text, status, file)" +
                        "values (?, ?, ?, ?, ?)");
                pr.setInt(1, note.getIdUser());
                pr.setDate(2, note.getDate());
                pr.setString(3, note.getText());
                pr.setString(4, note.getStatus());
                if (note.getNameFile() != null) {
                    pr.setString(5, note.getNameFile());
                } else {
                    pr.setString(5, "");
                }
                pr.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public void changeNoteStatus(int idNote, String status) {
        try {
            PreparedStatement pr = connection.prepareStatement("UPDATE notes" +
                    " set status = ? where id = ?");
            pr.setString(1, status);
            pr.setInt(2, idNote);
            pr.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (status.equals("ACTIVE")) {

            java.util.Date date = new java.util.Date();
            Date currentDate = new Date(date.getTime());

            try {
                PreparedStatement pr = connection.prepareStatement("UPDATE notes" +
                        " set status = ?, date = ? where id = ?");
                pr.setString(1, status);
                pr.setDate(2, currentDate);
                pr.setInt(3, idNote);
                pr.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void dropRecycleBin(int idUser) {
        ArrayList<Note> notes = new ArrayList<>();
        try {
            PreparedStatement pr = connection.prepareStatement("DELETE from notes" +
                    " where id_user = ? and status = ?");
            pr.setInt(1, idUser);
            pr.setString(2, "DELETED");
            pr.execute();
            setRecycleBin(notes);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void uploadFile(int IdNote, String fileName) {
        try {
            PreparedStatement pr = connection.prepareStatement("UPDATE notes" +
                    " set file = ? where id = ?");
            pr.setString(1, fileName);
            pr.setInt(2, IdNote);
            pr.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void dropFileFromNote(int IdNote) {
        String fileName = "";
        File file = new File(FILENAME + getFileName(IdNote));
        file.delete();
        uploadFile(IdNote, fileName);
    }

}
