package orm;


import model.User;

import java.sql.*;

public class UsersDAO extends DAO {
    Connection connection;
    PreparedStatement preparedStatementInsert;

    public UsersDAO() {
        try{
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Connection successful!");
        }
        catch(Exception ex){
            System.out.println("Connection failed...");
            System.out.println(ex);
        }

        try {
            connection = DriverManager.getConnection(DBURL, USERNAME, PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public String select() {
        StringBuilder sb = new StringBuilder();
        String string = new String();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * from users");

            while (resultSet.next()) {
                sb.append(resultSet.getInt(1));
                sb.append(" ");
                sb.append(resultSet.getString(2) + " ");
                sb.append(" ");
                sb.append(resultSet.getString(3) + " ");
            }

            string = sb.toString();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return string;
    }

    public void registrationUser(User user) {
        try {
            preparedStatementInsert = connection.prepareStatement("INSERT into " +
                    "users(username, password) " +
                    "values(?, ?) ");
            preparedStatementInsert.setString(1, user.getLogin());
            preparedStatementInsert.setString(2, user.getPassword());

            preparedStatementInsert.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean checkUserLogin(String login) {
        try {
            Statement statement = connection.createStatement();
            PreparedStatement pr = connection.prepareStatement("SELECT  * FROM users WHERE username = ?");
            pr.setString(1, login);
            ResultSet rs = pr.executeQuery();

            while (rs.next()) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public User getUserByAuthorization(String login, String password) {
        User user = null;
        try {
            if (login != null & password != null) {
                Statement statement = connection.createStatement();
                PreparedStatement pr = connection.prepareStatement(
                        "SELECT * from users WHERE username = ?" +
                        "AND password = ?");
                pr.setString(1, login);
                pr.setString(2, password);

                ResultSet rs = pr.executeQuery();

                while (rs.next()) {
                    user = new User(rs.getInt("idusers"),
                            rs.getString("username"),
                            rs.getString("password"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    public User getUserById(int id) {
        User user = null;
        String userName;
        String password;

        try {
            PreparedStatement pr = connection.prepareStatement("SELECT * from users " +
                    "WHERE idusers = ?");
            pr.setInt(1, id);
            ResultSet rs = pr.executeQuery();

            while(rs.next()) {
                userName = rs.getString("username");
                password = rs.getString("password");
                user = new User(userName, password);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

}
