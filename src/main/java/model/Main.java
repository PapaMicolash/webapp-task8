package model;

import orm.NotesDAO;

import javax.ws.rs.DELETE;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import static java.util.Arrays.*;


public class Main {

    public static void main(String[] args) throws IOException {
        NotesDAO notesDAO = new NotesDAO();

        System.out.println();


        notesDAO.sortNotesOnDate(2);

        ArrayList<Note> todayNotes = notesDAO.getTodayNotes();
        for (Note n : todayNotes) {
            System.out.println(n.toString());
        }

        System.out.println();
        ArrayList<Note> tomorrowNotes = notesDAO.getTomorrowNotes();
        for (Note n : tomorrowNotes) {
            System.out.println(n.toString());
        }

        System.out.println();
        ArrayList<Note> somedayNotes = notesDAO.getSomedayNotes();
        for (Note n : somedayNotes) {
            System.out.println(n.toString());
        }

        System.out.println();


        notesDAO.setRecycleBin(notesDAO.getNotesByIDUser(2, "DELETED"));
        ArrayList<Note> reb = notesDAO.getRecycleBin();
        for (Note n : reb) {
            System.out.println(n.toString());
        }

        notesDAO.dropRecycleBin(2);

        for (Note n : reb) {
            System.out.println(n.toString());
        }


        String s = "2020-02-12";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd");
        try {
            Date date = simpleDateFormat.parse(s);
            java.sql.Date sqlDate = new java.sql.Date(date.getTime());
            System.out.println(sqlDate.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }


        }




}




