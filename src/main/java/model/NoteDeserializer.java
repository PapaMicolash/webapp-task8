package model;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;

public class NoteDeserializer implements JsonDeserializer<Note> {

    @Override
    public Note deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        String pattern = "yyyy-MM-dd";
        String dateString1 = jsonElement.getAsJsonObject().get("dateInput").toString();
        String dateString = dateString1.substring(1, 11);
        System.out.println(dateString);
        String textString1 = jsonElement.getAsJsonObject().get("textInput").toString();
        String textString = textString1.substring(1, textString1.length() - 1);
        Date date = null;

        SimpleDateFormat format = new SimpleDateFormat(pattern);
        try {
            java.util.Date date1 = format.parse(dateString);
            date = new Date(date1.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Note note = new Note(date, textString);

        return note;
    }
}
