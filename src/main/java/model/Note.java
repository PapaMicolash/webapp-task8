package model;


import java.io.File;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Objects;

public class Note  {


    int id;
    int idUser;
    Date date;
    String text;
    String status;
    String nameFile;

    public Note(int id, int idUser, Date date, String text, String status, String nameFile) {
        this.id = id;
        this.idUser = idUser;
        this.date = date;
        this.text = text;
        this.status = status;
        this.nameFile = nameFile;
    }

    public Note(int id, int idUser, Date date, String text, String status) {
        this.id = id;
        this.idUser = idUser;
        this.date = date;
        this.text = text;
        this.status = status;
    }

    public Note(int idUser, Date date, String text, String status) {
        this.idUser = idUser;
        this.date = date;
        this.text = text;
        this.status = status;
    }

    public Note(Date date, String text) {

        this.date = date;
        this.text = text;

    }

    public Note() {
    }

    public int getIdUser() {
        return idUser;
    }

    public String getNameFile() {
        return nameFile;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean checkFields() {
        if (this.date == null) {
            return false;
        }
        if (this.text.isEmpty()) {
            return false;
        }
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Note)) return false;
        Note note = (Note) o;
        return id == note.id &&
                Objects.equals(date, note.date) &&
                Objects.equals(text, note.text) &&
                status == note.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, text, status);
    }

    @Override
    public String toString() {
        return "Note{" +
                "id=" + id +
                ", idUser=" + idUser +
                ", date=" + date +
                ", text='" + text + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
