function logOut(srvUrl) {
    var form = document.getElementById("out");
    form.action = srvUrl;
    form.submit();
}

function getUserName(srvUrl) {
    var xhrequest = new XMLHttpRequest();

    function showUserName() {
        if (xhrequest.readyState == 4) {
            var status = xhrequest.status;

            if (status == 200) {
                var divHead = document.getElementById("divHead");
                var userName = document.createElement("div");
                userName.className = "head1";
                userName.innerText = JSON.parse(xhrequest.responseText);
                divHead.appendChild(userName);

            } else {
                document.write("Something wrong here!" + xhrequest.statusText);
            }
        }
    }

    xhrequest.open("POST", srvUrl, true);
    xhrequest.onreadystatechange = showUserName;
    xhrequest.send();
}




function checkLogin(srvUrl) {
    var xhrequest = new XMLHttpRequest();

    function getPageStatus() {
        if (xhrequest.readyState == 4) {
            var status = xhrequest.status;
            if (status == 200) {
                var currentStatus = JSON.parse(xhrequest.responseText);

                switch (currentStatus) {
                    case 0:
                        document.getElementById("headerGuest").style.display = 'block';
                        break;
                    case 1:
                        document.getElementById("headerUser").style.display = 'block';
                        getNotes('/today');
                        getNotes('/tomorrow');
                        getNotes('/someday');
                        document.getElementById("notes").style.display = 'block';
                        getUserName('/getUserName');
                        break;
                    default:
                        alert("ERROR!!");
                        break;
                }
            } else {
                document.write("Ответ сервера " + xhrequest.statusText);
            }
        }
    }

    xhrequest.open("POST", srvUrl, true);
    xhrequest.onreadystatechange = getPageStatus;
    xhrequest.send();
}