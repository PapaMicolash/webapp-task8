function addNote() {
    var date = document.getElementById("dateInput");
    var text = document.getElementById("textInput");

    var xhrequest = new XMLHttpRequest();

    var note = {};
    note[date.id] = date.value;
    note[text.id] = text.value;

    var noteJSON = JSON.stringify(note);

    function getAddingStatus() {
        if (xhrequest.readyState == 4) {
            var status = xhrequest.status;
            if (status == 200) {
                var currentStatus = JSON.parse(xhrequest.responseText);
                alert(currentStatus);
                switch (currentStatus) {
                    case 1:
                        alert("Fill all fields!!!");
                        break;
                    case 2:
                        alert("This note added successfully!!!");
                        break;
                    default:
                        alert("ERROR!");
                        break;
                }
            } else {
                document.write("Ответ сервера " + xhrequest.statusText);
            }
        }
    }

    xhrequest.open("POST", "/addNote", true);
    xhrequest.onreadystatechange = getAddingStatus;
    xhrequest.send(noteJSON)
}

function hideCreateNote() {
    document.getElementById("createInput").style.display = 'none';
}

function createNewNote() {
    var form = document.getElementById("createrForm");

    var dateInput = document.createElement("input");
    dateInput.type = "date";
    dateInput.className = "create-date";
    dateInput.value = "Enter Date";
    dateInput.id = "dateInput";

    var noteInput = document.createElement("input");
    noteInput.type = "text";
    noteInput.className = "create-note";
    noteInput.placeholder = "Enter Note";
    noteInput.id = "textInput";


    var noteSubmit = document.createElement("input");
    noteSubmit.type = "submit";
    noteSubmit.className = "btn4";
    noteSubmit.value = "Add Note!";
    noteSubmit.setAttribute("onclick", "addNote()");


    form.appendChild(dateInput);
    form.appendChild(noteInput);
    form.appendChild(noteSubmit);

    hideCreateNote();
}


function getNotes(srvUrl) {
    var xhrequest = new XMLHttpRequest();

    function showNotes() {
        if (xhrequest.readyState == 4) {
            var status = xhrequest.status;

            if (status == 200) {
                var divName = 'tomorrow';
                if (srvUrl == '/today') {
                    divName = 'today';
                } else if (srvUrl == '/tomorrow') {
                    divName = 'tomorrow';
                } else if (srvUrl == '/someday') {
                    divName = 'someday';
                }

                var div = document.getElementById(divName);
                var notes = JSON.parse(xhrequest.responseText);

                for (var i = 0; i < notes.length; i++) {

                    var note = document.createElement("div");
                    note.className = "coverContent";

                    var dateText = document.createElement("div");
                    dateText.className = "content";
                    dateText.innerText = notes[i]["date"];
                    note.appendChild(dateText);

                    var text = document.createElement("div");
                    text.className = "content";
                    text.innerText = notes[i]["text"];
                    note.appendChild(text);

                    var fileDiv = document.createElement("div");
                    fileDiv.className = "fileContent";
                    note.appendChild(fileDiv);

                    var fileName = document.createElement("div");
                    fileName.className = "fileName";
                    fileDiv.appendChild(fileName);

                    var fileNameContent = document.createElement("div");
                    fileNameContent.className = "fileNameContent";
                    fileNameContent.innerText = notes[i]["nameFile"];
                    fileName.appendChild(fileNameContent);

                    var fileDivForm = document.createElement("div");
                    fileDivForm.className = "fileDivForm";
                    fileDiv.appendChild(fileDivForm);


                    var fileInput = document.createElement("input");
                    fileInput.type = "file";
                    fileInput.className = "file";
                    fileInput.name = "file" + notes[i]["id"];

                    fileDivForm.appendChild(fileInput);

                    var check = document.createElement("div");
                    check.className = "content23";
                    check.id = notes[i]["id"];

                    var fileSubmit = document.createElement("input");
                    fileSubmit.type = "button";
                    fileSubmit.className = "uploadFile";
                    fileSubmit.value = "Upload";
                    fileSubmit.setAttribute("onclick", "uploadFile()");
                    check.appendChild(fileSubmit);

                    var fileDownload = document.createElement("input");
                    fileDownload.type = "button";
                    fileDownload.className = "downloadFile";
                    fileDownload.value = "Download";
                    fileDownload.setAttribute("onclick", "downloadFile()");
                    check.appendChild(fileDownload);

                    var fileDrop = document.createElement("input");
                    fileDrop.type = "button";
                    fileDrop.className = "dropFile";
                    fileDrop.value = "Drop File";
                    fileDrop.setAttribute("onclick", "dropFile()");
                    check.appendChild(fileDrop);

                    var doneBox = document.createElement("input");
                    doneBox.className = "box1";
                    doneBox.value = "done!";
                    doneBox.type = "button";
                    doneBox.setAttribute("onclick", "getId('DONE')");

                    var deleteBox = document.createElement("input");
                    deleteBox.className = "box2";
                    deleteBox.value = "delete!";
                    deleteBox.type = "button";
                    deleteBox.setAttribute("onclick", "getId('DELETED')");

                    check.appendChild(doneBox);
                    check.appendChild(deleteBox);
                    note.appendChild(check);

                    div.appendChild(note);
                }
            } else {
                document.write("Ответ сервера sadifuhgasiugdf" + xhrequest.statusText);
            }

        }
    }

    xhrequest.open("POST", srvUrl, true);
    xhrequest.onreadystatechange = showNotes;
    xhrequest.send();
}

function sendStatus(srvURL) {
    var a = srvURL;
    var xhrequest = new XMLHttpRequest();
    function getReadyStateChange() {
        if (xhrequest.readyState == 4) {
            var status = xhrequest.status;
            if (status == 200) {

            } else {
                document.write("Ответ сервера " + xhrequest.statusText);
            }
        }

    }
    xhrequest.open("POST", "/findStatus", true);
    xhrequest.onreadystatechange = getReadyStateChange;
    xhrequest.send(a);
}