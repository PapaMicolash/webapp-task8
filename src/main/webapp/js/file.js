function dropFile() {
    findFile();
    var div = document.getElementById("upload");
    div.action = "/dropFile";
    div.submit();
}

function uploadFile() {
    var div = document.getElementById("upload");
    var idNote = document.getElementById("noteId");
    var a = event.target;
    idNote.value = a.parentNode.id;
    div.action = "/FileUpload";
    div.submit();
}

function downloadFile() {
    findFile();
    var div = document.getElementById("upload");
    div.action = "/FileDownload";
    div.submit();
}

function getId(srvURL) {
    sendStatus(srvURL);
    findFile();
    var div = document.getElementById("upload");
    div.action = "/changeStatus";
    div.submit();
}

function addFile() {
    var form = document.getElementById("fileAdder");
    form.action = "/fileUpload";
    form.submit();
}

function findFile() {
    var a = event.target;
    var xhrequest = new XMLHttpRequest();
    function getReadyStateChange() {
        if (xhrequest.readyState == 4) {
            var status = xhrequest.status;
            if (status == 200) {

            } else {
                document.write("Ответ сервера " + xhrequest.statusText);
            }
        }

    }
    xhrequest.open("POST", "/FileFinding", true);
    xhrequest.onreadystatechange = getReadyStateChange;
    xhrequest.send(a.parentNode.id);
}

